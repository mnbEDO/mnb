#!/usr/bin/env python3
from random import randint, seed
from sys import argv
from lutri import lutri
from descente import descente
from remonte import remonte
from debye import analyse_deb
from eqPB import convergeur
from newton import newton

import matplotlib.pyplot as plt

seed(666)


def main():
    if len(argv) > 1:
        n = argv[1]
    else:
        n = 5
    print("#########################")
    print("Tests sur matrices reeles")
    print("#########################")
    # FIXME : les randoms font parfois
    #         des matrices non-factorisables...
    a = [randint(-2, 7) for _ in range(n)]
    b = [randint(1, 4) for _ in range(n-1)]
    c = [randint(-5, 1) for _ in range(n-1)]
    z = [randint(0, 10) for _ in range(n)]
    print("-------------------------")
    print("a : ", a)
    print("b : ", b)
    print("c : ", c)
    print("z : ", z)
    print("-------------------------")
    l, v = lutri(a, b, c)
    print("l : ", l)
    print("v : ", v)
    y = descente(l, z)
    print("-------------------------")
    print("y : ", y)
    print("-------------------------")
    x = remonte(v, c, y)
    print("-------------------------")
    print("x : ", x)
    print("-------------------------")
    return 0

if __name__ == '__main__':
    main()
    #analyse_deb()
    plt.figure()
    plt.plot(*convergeur(3.7, mode14=True), "r-", label="Debye")
    plt.plot(*newton(3.7), "b-.", label="Newton")
    plt.legend()
    plt.savefig("mucompare.png")
