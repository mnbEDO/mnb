#!/usr/bin/env/ python3

def descente(l, z):
    """
    Effectue la dessente du systeme Ly=z
    :arg l: sous diagonale de L
    :arg z: second membre de l'equation
    :returns y: solution de l'equation
    """
    n = len(z)
    y = [0 for _ in range(n)]
    y[0] = z[0]
    for i in range(1, n):
        y[i] = z[i] - l[i-1]*y[i-1]
    return y
