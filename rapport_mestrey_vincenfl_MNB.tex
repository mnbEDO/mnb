\title{Compte rendu du TP de MNB}

\author{

         \\ Yannis Mestre
         Florian Vincent
                G8\\
}

\date{\today}


\documentclass[0.5pt]{article}
\usepackage[margin=0.5in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{pythontex}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage{dsfont}
\usepackage{float}

\begin{document}

\maketitle

\section*{Avant propos}

Ce compte rendu a été intégralement réalisé pendant le confinement avec les moyens du bord. Veuillez donc pardonner les divers problèmes de formatages dûs à \LaTeX.

Nous espérons tous deux que ce rapport validera les critères demandés en termes de présentation des résultats.

Il s'agit ici d'évaluer la solution à l'équation différentielle suivante, décrivant le potentiel électrique en fonction de la distribution de charges électriques autour d'un polymère chargé :

$$
(PB) : \left\lbrace\begin{array}{cll}
    \frac{d^2\Phi}{dr^2}+\frac{1}{r}\frac{d\Phi}{dr} &  = & \frac{2en_0}{\epsilon}sh\left(\frac{e\Phi}{k_BT}\right)
    \\ \Phi'(R) &  = & \frac{-\sigma}{\epsilon}
    \\ \Phi(r\rightarrow +\infty) & \rightarrow & 0
\end{array}\right.
$$

On note $R$ le rayon du polymère, que l'on considère cylindrique, et
$\Phi : \begin{array}{c} [R, +\infty[ \rightarrow \mathbb{R}^+ \\
r \rightarrow \Phi(r)\end{array}$ représente le potentiel électrique dans le repère cylindrique.
    On admet pour constante $T$ la température, $e=1,602 176 634.10^{-19}As$ la charge élémentaire, $\epsilon$ la permitivité dielectrique du milieu, et $n_0$ la concentration en charges infiniement loin du polymère.

Le tout forme le problème de l'équation de Poisson-Boltzmann (noté PB plus loin).
On abordera sa résolution par la méthode des approximations successives puis par des itérations de la méthode de Newton.

    Par la suite, on choisira de traiter le problème adimensionnel associé à (PB) en posant $x = \frac{r}{R}$ et $u(x) = \frac{-e}{k_B T}\Phi(Rx)$ pour traiter $\Phi$.
    On pose aussi $k^2 = \frac{2en_0}{R^2\epsilon}$ et $\mu = \frac{\sigma}{\epsilon}$

\section*{Réponses aux questions de l'ennoncé}

\subsection*{Factorisation LU sur matrices tridiagonales}

    \subsubsection*{1/ Formules de récurrence} %1

Soit $A$ une matrice tridiagonale.
On note $a$ sa diagonale, $b$ sa sous-diagonale, et $c$ sa sur-diagonale.
On cherche sa factorisation LU, dont on notera $l$ la sous diagonale de $L$ et $v$ la diagonale de $U$.

Par calcul matriciel, on obtenait en TD :
$$
\left\lbrace\begin{array}{lcl}
    a_k = l_k.c_k + v_k \\
    b_k = v_{k-1}.l_k \\
    c_k = c_k
\end{array}\right.
$$
Seulement, les indices des vecteurs $l$, $b$ et $c$ etaient décalés de $1$ à cette occasion.

On en déduisait le calcul effectif de la décomposition LU :
$$
\left\lbrace
\begin{array}{rcl}
    l_{k} & = & \frac{b_{k}}{v_k} \\
    v_k   & = & a_k - l_{k-1}.c_{k-1}
\end{array}\right.
$$
En partant de :
$$
\begin{array}{rcl}
    v_1 & = & a_1 \\
    l_1 & = & \frac{b_1}{a_1}
\end{array}
$$

    Pour ce qui est de l'algorithme associé à ce calcul, il suffit de décaler les coefficients (on utilise python donc l'indexage des tableaux commence à 0 comme il se doit).
Voir ci-dessous le pseudo-code \emph{Algorithm1 Decomposition LU}.

\begin{algorithm}

\caption{Decomposition LU}

    \begin{algorithmic}[1]

        \Procedure{lutri(a, b, c)}{}

            \State $l$ est un tableau de taille $n-1$
            \State $l$ est un tableau de taille $n$

            \State $v[1] \gets a[1]$
            \State $l[1] \gets b[1] / a[1]$
        \For{($i$ allant de $2$ à $n-1$)} :

            \State $v[i] \gets a[i] - (l[i-1] * c[i-1])$
            \State $l[i] \gets b[i] / v[i]$

        \EndFor

            \State $v[-1] \gets a[-1] - (l[-1] * c[-1])$

        \EndProcedure

        \State return $l$, $v$

    \end{algorithmic}

\end{algorithm}


    \subsubsection*{2/ Descente} %2

Par calcul matriciel, on notera que le système $Ly=z$ présente des équations de la forme :


$$
\begin{array}{rcll}
    y_i & = & z_i - l_{i-1}.y_{i-1} & \forall 1<i\le n \\
    y_1 & = & z_1 & \mbox{si i = 1}
\end{array}
$$

On calcule donc $y_1$ puis le reste des $y_i$.

On obtient un second algorithme (voir \emph{Algorithm2 $Ly=z$})


\begin{algorithm}

    \caption{$Ly=z$}

    \begin{algorithmic}[1]

        \Procedure{Descente(l, z)}{}

            \State $y$ est un tableau de taille $n$

            \State $y[1] \gets z[1]$
        \For{($i$ allant de $2$ à $n$)} :

            \State $y[i] \gets z[i] - (l[i-1] * y[i-1])$

        \EndFor

        \EndProcedure

            \State return $y$

    \end{algorithmic}

\end{algorithm}



    \subsubsection*{3/ Remontée} %3

On execute l'étape de "remontée" du système en résolvant le système $Ux=y$ à l'aide du vecteur $y$ obtenu lors de la descente.

Les formules de récurrence sur la descente sont les suivantes :

$$
\left\lbrace\begin{array}{rcll}
    x_n & = & y_n                           & i=n   \\
    x_i & = & \frac{y_i - c_i.x_{i+1}}{v_i} & \forall i<n
\end{array}\right.
$$


\begin{algorithm}

    \caption{$Ux=y$}

    \begin{algorithmic}[1]

        \Procedure{Remonter(v, c, y)}{}

            \State $x$ est un tableau de taille $n$

            \State $x[n] \gets y[n]$
        \For{($i$ allant de $1$ à $n$)} :

            \State $x[n-i] \gets \left(y[n-i] - (c[n-i] * x[n-i+1])\right) / v[n-i]$

        \EndFor

        \EndProcedure

            \State return $x$

    \end{algorithmic}

\end{algorithm}


\emph{Tests sur une matrice :}

On fera tourner les algorithmes précédents à l'aide de python3.
La suite du TP fera aussi usage de numpy, ou de matplotlib parfois.

On emploie la matrice $A = \left(\begin{array}{ccccc}
    5 & 1 & 0 & 0 & 0 \\
    1 & 4 &-4 & 0 & 0 \\
    0 & 3 & 4 &-2 & 0 \\
    0 & 0 & 1 & 2 & 0 \\
    0 & 0 & 0 & 1 & 6 \end{array}\right)$ pour résoudre le système d'équations associé.

On veut résoudre $A\vec{x} = \vec{z}$ dans $\mathbb{R}^{5}$. Ici on prendra
$\vec{z}=\left(\begin{array}{c} 7\\ 9\\ 3\\ 0\\ 1\end{array}\right)$.

On obtient comme résultat :
\begin{lstlisting}[language=bash]
[mestreyvincenfl/home/MNB/TP/]\$ ./main.py
#########################
Tests sur matrices reeles
#########################
-------------------------
a :  [5, 4, 4, 2, 6]
b :  [1, 3, 1, 1]
c :  [1, -4, -2, 0]
z :  [7, 9, 3, 0, 1]
-------------------------
l :  [0.2, 0.7894736842105263,
      0.13970588235294118,
      0.43870967741935485]
v :  [5, 3.8, 7.157894736842105,
      2.2794117647058822, 6.0]
-------------------------
y :  [7, 7.6, -3.0,
      0.41911764705882354,
      0.8161290322580645]
-------------------------
-------------------------
x :  [1.0774193548387097,
      1.6129032258064517,
      -0.36774193548387096,
      0.1838709677419355,
      0.13602150537634408]
-------------------------
Done.
\end{lstlisting}


\rule{8cm}{0.4pt}

\subsection*{Problème de Debye-Hückel}

    \subsubsection*{4/ Inversibilité de A} %4

On tentera de montrer que $A$ est à diagonale strictement dominante.

Pour la première ligne :
$h^2 + 2 > 2$ car $h>0$.

Pour les $i-1$ prochaines lignes :
\begin{itemize}
\item Pour $h<2x_i$ on a $|1-\frac{h}{2x_i}|=1-\frac{h}{2x_i}$.

Dans le premier cas :
$$
|1-\frac{h}{2x_i}|+|1+\frac{h}{2x_i}| = 2 < h^2 + 2
$$

\item Pour $h>2x_i$ on a $|1-\frac{h}{2x_i}|=\frac{h}{2x_i}-1$.

Dans le second cas :
$$
|1-\frac{h}{2x_i}|+|1+\frac{h}{2x_i}| = \frac{h}{x_i}
$$
\end{itemize}

Pour vérifier l'inégalité dans ce second cas, on dérive la différence, soit $p(h) = h^2 - \frac{1}{x_i}h + 2$:

$$
p'(h)=0 \Leftrightarrow 2h-\frac{1}{x_i}=0 \Leftrightarrow h=\frac{1}{2x_i}
$$
Or $p(\frac{1}{2x_i})=\frac{1}{4x_{i}^2} - \frac{1}{2x_{i}^2} + 2 = 2 - \frac{1}{4x_{i}^2}\ge \frac{3}{4} > 0$ car $x_i \ge 1$.
On obtient donc bien dans le second cas $\frac{h}{x_i} < h^2 + 2$

Conclusion : pour tout $x_i$, $A$ est à diagonale strictement dominante.

Donc $A$ est inversible et admet une décomposition LU.
$\blacksquare$

    \subsubsection*{5/ Calcul de la fonction} %5

On emploie pour l'approximation de la fonction $u(x)$ l'algorithme de décomposition LU vu précédemment.

On travaille ici sur la matice $A$ de la forme suivante:

$$
A =
\begin{bmatrix}
    -(2+h^2)           & 2 & 0 & \dots & 0 \\
    \\
    (1-\frac{h}{2x_1}) & -(2+h^2) & (1+\frac{h}{2x_1}) & \ddots & \vdots  \\
    \\
    0                  & (1-\frac{h}{2x_2}) & \ddots & \ddots & 0 \\
    \\
    \vdots & \ddots & \ddots & \ddots & (1+\frac{h}{2x_{n-2}}) \\
    \\
    0 & \dots & 0 & (1-\frac{h}{2x_{n-1}}) & -(2+h^2)
\end{bmatrix}
$$

Étant donnée une n-partition de $[1; 1+l]$ par des $x_i$ équidistants formant un vecteur $X$, on peut obtenir les $u_i$ par résolution du système $AU=Z$. Dans notre cas, on a posé $Z =  ^T\begin{bmatrix} \mu h(h-2), & 0, & \dots, & 0 \end{bmatrix}$.

Pour l'instant, on a posé $\mu = 1$ et $l=10$.

Pour calculer les $u_i$, on applique l'algorithme suivant :


\begin{algorithm}

    \caption{Algo pour Debye-Hückel}

    \begin{algorithmic}[1]

        \Procedure{Decomposition LU}{}

            \State $X$ est la partition de $[1; 1+l]$
            \State $a$, $b$, $c$ sont des vecteurs de tailles $n$, $n-1$ et $n-1$
            \State Le premier élément de $c$ est un $2$
            \State $Z$ contient $h(h-2)$ puis que des $0$
            \State $a$ contient $-2-h^2$ dans toutes ses cases

        \For{($x_i$ point de $X$ ($\neq 1$))} :

            \State $add = \frac{h}{2x_i}$

            \If{(i==n)}:
                \State Ajouter $1-add$ en queue de $b$
            \Else :
                \State Ajouter $1-add$ en queue de $b$ et $1+add$ en queue de $c$
            \EndIf

        \EndFor


        \State $l, v = lutri(a, b, c)$

        \EndProcedure

        \Procedure{Resolution}

            \State $y = descente(l, z)$

            \State $U = remonte(v, c, y)$

        \EndProcedure

            \State return $(X, U)$

        \State Affiche le graphe de $U(X)$

    \end{algorithmic}

\end{algorithm}

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=15cm]{graphede.png}
    \end{center}
    \caption{Graphe de $u(x)$ pour $f=Id_{\mathbb{R}\rightarrow\mathbb{R}}$}
    \label{fig:Identite}
\end{figure}\end{center}

    \subsubsection*{6/ Précision en fonction du pas de calcul} %6

Comme $u_n$ est fixé à $0$ dans les calculs préliminaires, de fait sa valeur est nulle une fois l'algorithme lancé.

En revanche, $u_0$ est le plus suceptible de souffrir d'un pas de calcul trop élevé. On obtient ci-contre la valeur de $u_0$ et de $u_n$ en fonction de h (pour des $n$ variés, de 10 à 1000).

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=15cm]{graphn.png}
    \end{center}
    \caption{$u_0$ et $u_n$ en fonction de $h$}
    \label{fig:Effeth}
\end{figure}\end{center}

\noindent\rule{8cm}{0.4pt}

\subsection*{Itérations de la méthode des approximations successives}

    \subsubsection*{7/ Calcul recursif des $u^{(k)}$} %7

On considère maintenant que la fonction $f$ n'est plus l'identité mais le sinus hyperbolique. Le module math de python propose cette fonction, on s'en sert donc pour créer la fonction $g(x) = sh(x)-x$.

On obtient $u^{(k+1)}$ par résolution du système $Au^{(k+1)}=z(u^{(k)})$ avec $z$ le second membre.
$$
z_i = h^2g(u_{i}^{(k)}) + \mathds{1}_{i=0}\mu h(h-2)
$$
On note ce membre de droite $G(u^{(k)})$ à présent.

Il suffit d'ajuster l'algorithme précédent pour les valeurs de $z$, et on obtient un calcul fidèle de $u^{(k+1)}$ en fonction de $u^{(k)}$.

Sachant que $u^{(0)}$ est obtenu avec le premier algorithme (\emph{Algo pour Debye-Hückel}, voir ci-dessus), la relation de récurence permet d'itérer la méthode autant que voulu.

\subsubsection*{8/ Itérations jusqu'à un seuil de tolérence} %8

On se fixe deux garde-fous pour les itérations de la méthode.

Le premier est un nombre d'itérations maximal : $k_0 = 200$.

Le second est une majoration arbitraire de l'écart entre les deux dernières itérations calculées : $||Au^{(k_0)} - G(u^{(k)})||_{\infty} < \eta_1=10^{-12}$ et $||u^{(k_0)} - u^{(k_0-1)}||_{\infty}<\eta_2=10^{-9}$.

Pour vérifier ce deuxième point, on stocke les $u^{(k)}$ dans un tableau et on calcule les normes grâce aux deux derniers éléments. On notera que $Au^{(k)}$ peut être à peu près calculé à l'aide de $G(u^{(k-1)})$ (modulo l'expression $\mu h(h-2)$ de $u_0$).

Commençons par évaluer la fonction pour $\mu=1$ et $\mu=4$.

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=12cm]{mu1et4.png}
    \end{center}
    \caption{$\mu=1$ et $\mu=2$}
    \label{fig:mu1et4}
\end{figure}\end{center}

    \subsubsection*{9/ Variations de $\mu$ et effets sur le schéma} %9

On peut tracer les $u^{(k_0)}_i$ en fonction des $x_i$ pour plusieurs valeurs de $\mu$ entre $0$ et $7$.
Pour les $\mu\in\mathbb{N}$ on obtient la courbe ci-dessous.
On notrera que pour les courbes qui suivront, on affichera en dessous le graphe de la première itération $u^{(0)}$, en violet, pour voir l'évolution du schéma.

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=15cm]{mu0to7.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu\in\lbrace 0, ..., 7\rbrace$, avec $u^{(0)}_i$, en fonction des $x_i$}
    \label{fig:mu0to7}
\end{figure}\end{center}

On observe sur les courbes colorées que la fonction $u$ a l'air de croître avec $\mu$ (voir Figure\ref{fig:mu0to7}).

L'algorithme ne converge pas au même rythme en fonction de $\mu$ en revanche. En effet, le cran d'arrêt $k_0$ pour les itérations du schéma sont $1$, $7$, $12$, $22$, $49$, $200$ et $200$ respectivement pour $\mu=0$, $1$, $2$, $3$, $4$, $5$ et $6$.
Pour $\mu=7$, le schéma diverge si rapidement que le sinus hyperbolique du module math de python lève un overflow dès la première itération.
De fait, pour un garde-fou placé à $k_0=200$ itérations, le schéma diverge pour $\mu\ge 5$.
On ne le voit pas très bien pour $\mu=5$ graphiquement, mais c'est plus évident pour $\mu=6$ dont la courbe descend en dessous de $0$.

On peut s'attarder un court instant sur le cas $\mu=0$ qui converge sans itérer.
Lors de la mise en pratique du premier schéma, le second membre est ici nul. Or on sait $A$ inversible. Donc $kerA=\lbrace 0\rbrace$. On en déduit immédiatement que $u^{(0)}=0$.
Avec le même argment, on comprends que $u^{(1)}=0$ lors de la premère itération, ce qui mettra terme au calcul par le critère d'infériorité à $\eta_1$.


On peut alors chercher à partir de quelle valeur de $\mu$ le schéma diverge.
Pour cela, on verifie les valeurs flottantes de $\mu$ tous les $0.02$, afin d'affiner notre analyse.
On obtient la figure suivante:


\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=10cm]{mu0to7prec50.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu\in\lbrace 0, 0.02, ..., 6.98, 7\rbrace$, en fonction des $x_i$}
    \label{fig:mu0to7prec50}
\end{figure}\end{center}


Pour ce qui est des résultats de l'algorithme, $k_0$ atteint $200$ quand $\mu=4.8$:

\begin{lstlisting}[language=bash]
mu4.76: break2
k0= 177
mu4.78: break2
k0= 189
mu4.8: k0= 200
mu4.82: k0= 200
\end{lstlisting}


On en conclut que le schéma ne converge pas pour toute valeur de $\mu$.
Visiblement, lorsque $\mu$ dépasse $6$, le schéma diverge très rapidement.

Cependant, en fixant $k_0$ à $500$, on peut laisser le schéma pour $\mu=6$ converger.


\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=10cm]{mu0to7prec1k500.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu\in\lbrace 0,...,7\rbrace$, en fonction des $x_i$ pour $k_0=500$}
    \label{fig:mu0to7k500}
\end{figure}\end{center}

En affinant l'analyse sur $\mu$, on voit que pour $k_0=500$ le schéma semble converger plus facilement.
Le sinus hyperbolique ne tient plus lorsque $\mu$ atteint $6.5$, et lève toujours une erreur d'overflow.


\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=10cm]{mu0to7prec50k500.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu\in\lbrace 0, 0.02, ..., 6.48\rbrace$, en fonction des $x_i$ pour $k_0=500$}
    \label{fig:mu0to7p50k500}
\end{figure}\end{center}

Finalement, on observe ci-dessous les différentes courbes des $u^{(k)}$, montrant l'évolution du résultat au cours des itérations.
On choisit arbitrairement $\mu=4.2$, qui nous donne des courbes lisibles.
Le schéma converge après $61$ itérations, on n'affiche qu'une courbe sur dix ainsi que les 3 premières.

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=17cm]{muuk.png}
    \end{center}
    \caption{Differents $u^{(k)}$ pour un schéma avec $\mu=4.2$}
    \label{fig:muuk}
\end{figure}\end{center}

On voit rapidement que le schéma oscille autour d'une solution finale.
C'est un résultat intéressant, car au lieu des garde-fous décrits précédemment, on aurait pû imaginer qu'il suffirait que la croissance de la courbe vis-à-vis des itérations ralentisse pour avoir la courbe finale.
Mais la courbe monte puis redescend comme on le voit sur la figure précédente.
On doit donc attendre que la courbe diverge ou reste arbitrairement proche de la courbe précédente.

\subsection*{Méthode de Newton}
    \subsubsection*{10/ Calcul et étude de l'inversibilité de la jacobienne} %10

Pour définir la jacobienne de la fonction $F$, on travaillera coefficient par coefficient.
Avec $(J_k)_{i, j} = \frac{\partial F_i}{\partial u^{(k)}_j}$ on obtient le système (absolument illisible) suivant en réalisant les dérivées partielles:
$$
\left\lbrace \begin{array}{ccc}
    i=0: & \frac{\partial F_0}{\partial u^{(k)}_j}= & \left\lbrace\begin{array}{cc}
                                                    j = 0: & -2 - h^2ch(u^{(k)}_0)\\
                                                    j = 1: & 2 \\
                                                    ailleurs: & 0
                                                \end{array}\right.\\
    i\in [1, n-2] & \frac{\partial F_i}{\partial u^{(k)}_j}= & \left\lbrace\begin{array}{cc}
                                                    j = i-1: & 1-\frac{h}{2x_i} \\
                                                    j = i: & -2 - h^2ch(u^{(k)}_i)\\
                                                    j = i+1: & 1+\frac{h}{2x_i} \\
                                                    ailleurs: & 0
                                                \end{array}\right.\\
    i=n-1: & \frac{\partial F_{n-1}}{\partial u^{(k)}_j}= & \left\lbrace\begin{array}{cc}
                                                    j = n-2: & 1-\frac{h}{2x_{n-1}} \\
                                                    j = n-1: & -2 - h^2ch(u^{(k)}_{n-1})\\
                                                    ailleurs: & 0
                                                \end{array}\right.
\end{array}\right.
$$
On peut le résumer en écrivant la forme de la matrice $J_k$ en fonction de $u^{(k)}$ explicitement :
$$
J_k(u^{(k)}) =
\begin{bmatrix}
    -(2+h^2ch(u^{(k)}_0))         & 2 & 0 & \dots & 0 \\
    \\
    (1-\frac{h}{2x_1}) & -(2+h^2ch(u^{(k)}_1)) & (1+\frac{h}{2x_1}) & \ddots & \vdots  \\
    \\
    0                  & (1-\frac{h}{2x_2}) & \ddots & \ddots & 0 \\
    \\
    \vdots & \ddots & \ddots & \ddots & (1+\frac{h}{2x_{n-2}}) \\
    \\
    0 & \dots & 0 & (1-\frac{h}{2x_{n-1}}) & -(2+h^2ch(u^{(k)}_{n-1}))
\end{bmatrix}
$$

On avait montré que la même matrice sans les cosinus hyperboliques était inversible car sa diagonale était strictment dominante.
Avec l'ajout du cosinus hyperbloique, c'est toujours vrai car $ch(x)\ge 1$ $\forall x\in\mathbb{R}$.\\
On obtient donc que $J_k$ est inversible pour tout $k$.  $\blacksquare$

    \subsubsection*{11/ Itérations de la méthode de Newton} %11

On calcule $u^{(0)}$ à l'aide des résultats de la partie 5.
On itére ensuite le calcul de $u^{(k+1)}$ en fonction de $u^{(k)}$ en calculant $J_k$,
puis en réalisant la résolution du système $J_k(u{(k)}-u^{(k+1)}) = J_kr = z$ (avec $z = F(u^{(k)})$ et $r=u^{(k)}-u^{(k+1)}$ par décomposition LU.
On obtient ensuite $u^{(k+1)}=u^{(k)}-r$.\\
Pour créer l'alogithme de calcul des itérations, on enregistrera dans la variable $res$ le resultat de la résolution du système linéaire ($r$ plus haut).\\
On fixe comme précédemment des garde-fous pour limiter le nombre d'itérations,
on impose donc un maximum absolu de 50 itérations (on verra plus tard que cette méthode converge nettement plus vite que la précédente), après quoi on considère que la méthode diverge.
De plus, si les itérations se rapprochent les unes des autres, on considère que l'algorithme a convergé.
On mesure cette convergence avec les deux critères suivants : $||F(u^{(k)})||_{\infty}<\eta_1$ et $||u^{(k)}-u^{(k-1)}||_{\infty}<\eta_2$ avec $\eta_1=10^{-12}$ et $\eta_2=10^{-9}$.


L'algorithme permettant ce calcul, ici noté \emph{NEWTON($\mu$, $n$)}, est le suivant:

\begin{algorithm}[H]

    \caption{Méthode de Newton}

    \begin{algorithmic}[1]

        \Procedure{Calcul de $F(x)$}{}

            \State $F_0 = -2u^{(k)}_0 - h^2sh(u^{(k)}_0 + 2u^{(k)}_1 + \mu h(h-2)$

            \State i=1

            \While{($i<n-1$)} :

                \State $F[i] = (1-\frac{h}{2x_i})u^{(k)}_{i-1} - 2u^{(k)}_i - h^2sh(u^{(k)}_i) + (1+\frac{h}{2x_i})u^{(k)}_{i+1}$

                \State i++

            \EndWhile

            \State $F_{n-1} = (1-\frac{h}{2x_i})u^{(k)}_{n-2} -2u^{(k)}_{n-1} - h^2sh(u^{(k)}_{n-1})$

        \EndProcedure

    \State return $F$

        \Procedure{Compute}{$u^{(k+1)}$($u^{(k)}$)}

            \State $J_k =
                \begin{bmatrix}
                    -(2+h^2ch(u^{(k)}_0))         & 2 & 0 & \dots & 0 \\
                    \\
                    (1-\frac{h}{2x_1}) & -(2+h^2ch(u^{(k)}_1)) & (1+\frac{h}{2x_1}) & \ddots & \vdots  \\
                    \\
                    0                  & (1-\frac{h}{2x_2}) & \ddots & \ddots & 0 \\
                    \\
                    \vdots & \ddots & \ddots & \ddots & (1+\frac{h}{2x_{n-2}}) \\
                    \\
                    0 & \dots & 0 & (1-\frac{h}{2x_{n-1}}) & -(2+h^2ch(u^{(k)}_{n-1}))
                \end{bmatrix}$

            \State a = diagonale($J_k$)

            \State b = sous-diagonale($J_k$)

            \State c = sur-diagonale($J_k$)

            \State l, v = lutri($J_k$)

            \State y = descente(l, F($u^{(k)}$))

            \State res = remonte(v, c, y)

        \EndProcedure

    \State return $u^{(k)} - res$

        \Procedure{Newton}{$\mu$, $n$}

            \State $u^{(0)} = Debye(Id)$

            \State k = 0

            \While{True}:

                \State $u^{(k+1)}$ = Compute($u^{(k+1)}$($u^{(k)}$))

                \If{($||F(u^{(k)})||_{\infty}<\eta_1$ and $||u^{(k)}-u^{(k-1)}||_{\infty}<\eta_2$)}:

                    \State $k_0=k$

                    \State Break

                \EndIf

                \If{($k>50$)}:

                    \State $k_0=k$

                    \State Break

                \EndIf

                \State k++

            \EndWhile

        \EndProcedure

    \State Affiche $u^{(k_0)}(x)$

    \end{algorithmic}

\end{algorithm}

On obtient alors une courbe pour $\mu=1$ et $\mu=4$ :

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=17cm]{mu1et4Newton.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu=1$ et $\mu=4$, avec la méthode de Newton}
    \label{fig:mu1et4Newton}
\end{figure}\end{center}

Les deux courbes ont convergé respectivement à 2 et 4 itérations.
C'est très court comparé à la méthode précédente qui nécessitait respectivement 7 et 49 itérations.
Cette méthode est visiblement bien plus rapide pour cette fonction.\\
Pour tenter d'expliquer cette efficacité remarquable, on peut remarquer que $u^{(0)}$ est déjà très proche de la solution $u^{(k_0)}$.
En effet, l'algorithme de la méthode de Newton a tendance à observer une convergence assez lente et bégayante si il ne démare pas assez proche d'un zero de la fonction $F$.
Mais la méthode de Debye calculant l'élément initial $u^{(0)}$ a visiblement le bon goût d'être assez fidèle au modèle et proche de la solution réelle.

    \subsubsection*{12/ Étude de l'effet de $\mu$ sur la convergence} %12

Comme précedemment, on étudie le graphe de la solution pour $\mu\in [0, 7]$.

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=17cm]{mu0to7NewtonpPas1.png}
    \end{center}
    \caption{$u^{(k_0)}_i$ pour $\mu\in{0, ...,7}$, avec la méthode de Newton}
    \label{fig:mu0to7Newton}
\end{figure}\end{center}

Cette fois-ci, le cosinus hyperbolique et le sinus hyperboliques n'ont pas subi d'overflow.
La méthode a convergé pour toutes les valeurs de $\mu$ entre 0 et 7.
On obtient les valeurs de $k_0$ en fonction de $\mu$ suivantes :
\begin{lstlisting}[language=bash]
mu0.0: k0= 0
mu1.0: k0= 2
mu2.0: k0= 3
mu3.0: k0= 3
mu4.0: k0= 4
mu5.0: k0= 4
mu6.0: k0= 5
mu7.0: k0= 5
\end{lstlisting}

Elles sont étonnamment faibles.\\
En cherchant à partir de quelle valeur de $\mu$ la méthode diverge,
il faut attendre $\mu=79$ comme on le voit ci-dessous :

\begin{lstlisting}[language=bash]
mu77: k0= 50
mu78: k0= 50
mu79: k0= 51
mu80: k0= 51
\end{lstlisting}

Pour illustrer la vitesse de la convergence, on peut étudier les différentes courbes des $u^{(k)}$ sur leur voie vers la convergence.
On obtient la figure suivante :
\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=20cm]{muukNewton.png}
    \end{center}
    \caption{$u^{(k)}_i$ en fonction des $x_i$ pour $\mu=60$, avec la méthode de Newton}
    \label{fig:muukNewton}
\end{figure}\end{center}

Contrairement à la méthode itérative que l'on avait étudié plus tôt, la méthode de Newton n'oscille pas autour de la solution $u^{(k_0)}$.
Elle décroit rapidement (elle suit en fait une convergence quadratique), mais on ne sait pas encore si elle converge vers la même courbe que la méthode précédente.

\section*{Conclusions}

Une fois ces calculs effectués, on peut conclure sur les résultats obtenus.

\subsection*{Comparaison des deux méthodes}

On compare donc les deux courbes pour la valeur de $\mu$ arbitraire de $3.7$.\\
Voici ci-dessous la comparaison des deux courbes :

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=20cm]{mucompare.png}
    \end{center}
    \caption{$u^{(k)}_i$ en fonction des $x_i$ pour $\mu=60$, avec la méthode de Newton}
    \label{fig:mucompare}
\end{figure}\end{center}

On remarque visuellement qu'elles sont presque confondues, en fait l'écart entre les deux doit probablement être du même ordre de grandeur que $\eta_2$.
Si on devait discuter de l'efficacité des deux méthodes de calcul, il ne s'agirait donc pas de savoir ce vers quoi elles convergent mais à quelle vitesse.\\
Le point de convergence est assurément la distribution correcte des charges autour de notre polymère chargé, mais la méthode de Newton converge plus vite et avec un coût asymptotique restant faible.
C'est donc ce résultat qui se trouve être le plus intéressant.

\subsection*{Application à des grandeurs physiques réelles}

On peut concrétiser les résultats de la méthode avec des données numériques décidées préalablement.
On choisit la méthode de Newton, et les contraintes suivantes :
\begin{enumerate}

    \item La permittivité diélectrique du vide est $\epsilon_0=8,854187.10^{-12}Fm^{-1}$.
On se placera en milieu aquatique, de permittivité relative $\epsilon_r=78.5$.
        La permittivité du milieu est donc $\epsilon=6.9505368.10^{-10}Fm^{-1}$.

    \item On considère un polymère de rayon $100nm$ soit $R = 10^{-7}m$.

    \item On a pris pour les applications numériques $k = 1$, on garde donc cette donnée.

    \item On travaille avec du PVC. Ce polymère est produit sous forme d'agglomérats d'environ $0.2\mu m$.
        La densité surfacique de charges d'un tel agglomérat est d'environ $\sigma = 2.10^{-2} C.m^{-2}$.

\end{enumerate}

Avec ces constantes, on ajoute que $e = 1,602176634.10^{-19}As$ et $n_0=\frac{\epsilon R^2}{2e}=2.16909193.10^{-9}$ car $k^2=1$.
On précise aussi la valeur de la constante de Boltzmann $k_B=1,380649.10^{-23}JK^{-1}$.\\

On obtient donc $\mu=\frac{\sigma}{\epsilon}=2.2588183421.10^{9}$.\\

On a déjà vu que la méthode de Newton convergeait facilement et rapidement pour $\mu\in[0, 7]$.
On considère donc le résultat de la méthode de Newton pour $\mu=2.2588183421$.

On sait que le résultat de l'algorithme a pour pente $\mu$ en $x=1$,
on n'aura donc qu'à multiplier le résultat de la méthode par $10^{9}$ une fois le calcul terminé pour avoir une bonne approximation du résultat final,
au moins au voisinage de $r=R$ qui est la zone la plus intéressante.\\

Ce qui nous intéresse est la distribution de boltzmann :
$$
n_+(r) = n_0e^{\frac{-e\Phi(r)}{k_BT}}
$$
$$
n_-(r) = n_0e^{\frac{e\Phi(r)}{k_BT}}
$$

On trace ci-dessous, grâce aux outils 3D de matplotlib, la distribution des charges positives en rouge, et la distribution des charges negatives en bleu.
Le polymère chargé est représenté en vert.

\begin{center}\begin{figure}[H]
    \begin{center}
        \includegraphics[width=20cm]{3Dions.png}
    \end{center}
    \caption{Distribution des ions autours d'un agglomérat cylindrique de PVC chargé.}
    \label{fig:ions}
\end{figure}\end{center}

$\square$
\end{document}
