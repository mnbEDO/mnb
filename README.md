# README #

* * *

## Ce projet sera fait en python et en latex si dieu le veut. Point.

### Ici on peut mettre toutes les remarques qu'on veut se faire l'un à l'autre.

Documente tout ce que tu veux sur les modifs que tu fais

> ~~Attention : je ne lis jamais les messages de tes commits, je sais même pas comment on fait.~~
>   Stp met de temps en temps un petit mot ici, ça prends 2s à peine.

* 25/03 commit pour LU pas vérifié
* Fait sur internet :
$$
> 4x_1 −2x_2 = 6
> 3x_1 + 5x_2 + x_3 = 7
> 4x_2 + x_3 −5x_4 = 9
> x_3 −2x_4 −4x_5 = 3
> 2x_4 − x_5 = 9
$$
> solution : x1 = 53/3, x2 = 97/3, x3 = -623/3, x4 = -262/15, x5 = -659/15
> Le programe main.py donne le bon résultat (le 27/03)
* Commit de l'exercice 2.2 SANS avoir mis à jour le .tex, à faire plus tard.

* Commit de la question 7 à la 9. le .tex est à jour. (12/04)

* Commit de Newton + début interprétation physique. (21/04)

* Fin de l'interprétation physique. (22/04)

### Ici, on mettra les bugs à corriger

On peut lister les trucs qui marchent pas en mettant le numero de la ligne.
 + Dans main.py (l.18-21) les randoms font parfois des matrices non inversibles (donc zerodivisionerror)

### Liste des programmes à lancer si tu veux compiler le .tex

- main.py
    * entier n en argument (taille de la matrice à creer)

- eqPB.py
    * entier n en argument (pas de cgt de $\mu$)
        + Si n=0 lance un graphe des $u^{(k)}$

- physique.py

* * *

## Gros zoubi
