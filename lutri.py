#!/usr/bin/env python3

def lutri(a, b, c):
    """
    Decomposition LU d'une matrice tridiagonale.

    :arg a: Diagonale
    :arg b: Sous diagoinale
    :arg c: Sur diagonale
    :returns l: sous diagonale de L
    :returns v: Diagonale de U

    Note : Diagonale de L     = 1
           Sur diagonale de U = c
    """
    n = len(a)
    l = [0 for _ in range(n-1)]
    v = [0 for _ in range(n)]
    v[0] = a[0]
    l[0] = b[0] / a[0]
    for i in range(1, n-1):
        v[i] = a[i] - (l[i-1] * c[i-1])
        l[i] = b[i] / v[i]
    v[-1] = a[-1] - (l[-1] * c[-1])
    return (l, v)

"""
$$
\begin{array}{rcl}
l_{k} & = & \frac{b_{k}}{v_k} \\
v_k   & = & a_k - l_{k-1}.c_{k-1}
\end{array}
$$
"""
