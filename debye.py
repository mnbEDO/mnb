#!/usr/bin/env python3

from lutri import lutri
from descente import descente
from remonte import remonte
import numpy as np
import matplotlib.pyplot as plt


def debyeId(mu=1, n=1000, l=10, plot=True, Z=None):
    """
    Calcule les ui pour f(u) = u avec la methode de debeye
        sur le segment [1; l+1]
    On peut donner a la fonction une autre fonction f en donnant
        le second membre Z. Si rien n'est donne, l'identite est utilisee.
    On pose les parametres :
    :arg mu: mu=1    constante.
    :arg n:  n=1000  finesse du decoupage
    :arg l:  l=10    longueur du segment a decouper

    :returns X:      Decoupage effectue
    :returns U:      Vecteur solution
    """

    # Constantes
    h = l / n
    h2= h * h

    # Creation du vecteur X
    i = 0
    x = 1
    X = [0 for _ in range(n)]
    while i < n:
        X[i] = x  # X[1] = x_1 and so on
        x += h
        i += 1

    # Initialisation de la matrice A
    #   et du vecteur z
    b = []
    a = [-2 - h2 for _ in range(n)]
    c = [2]
    if Z:
        z = Z
    else:
        z = [0 for _ in range(n)]
        z[0] = mu * h * (h-2)
    for xi in X[1:]:  # On commence a x_1 car on veut x>1
        add = h / (2 * xi)
        b.append(1 - add)
        c.append(1 + add)
    c.pop()  # Attention : x_{n-1} n'apparait pas dans c

    # Calcul de la decomposition
    l, v = lutri(a, b, c)

    # Descente puis remontee
    y = descente(l, z)
    U = remonte(v, c, y)

    # Plots
    if plot:
        plt.figure()
        plt.plot(X, U)
        plt.savefig("graphede.png")

    return (X, U)

def analyse_deb():
    """
    Tests
    """
    debyeId()
    h, u0, un = [], [], []
    # assoc = ((10, 'ro'), (100, 'b+'), (1000, 'g--'), (5000, 'c'))
    assoc = range(10, 1000)
    plt.figure()
    for n in assoc:
        h.append(10 / n)
        U = debyeId(mu=1, n=n, l=10, plot=False)[1]
        u0.append(U[0])
        un.append(U[-1])
    plot0 = plt.plot(h, u0, 'c+', label='u_0')
    plotn = plt.plot(h, un, 'bo', label='u_n')
    plt.legend()
    plt.savefig("graphn.png")


if __name__ == '__main__':
    analyse_deb()
