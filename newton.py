#!/usr/bin/env python3

from debye import debyeId
from descente import descente
from remonte import remonte
from lutri import lutri
from math import cosh, sinh

import matplotlib.pyplot as plt
import sys as sys


def F(uk, dico):
    """
    Args : uk, X, h, h2, mu, n
    :returns F: out = F(u^{(k)})
    """
    X = dico['X']
    h = dico['h']
    h2 = dico['h2']
    mu = dico['mu']
    n = dico['n']
    # F0:
    F = [-2 * uk[0] - h2*sinh(uk[0]) + 2 * uk[1] + mu * (2*h-h2)]
    i = 1
    # Fi:
    while i < n-1:
        F.append((1 - h/(2*X[i-1]))*uk[i-1] - 2*uk[i] - h2*sinh(uk[i])\
                + (1+h/(2*X[i-1]))*uk[i+1])
        i += 1
    # Fn-1
    F.append((1 - h/(2*X[n-2]))*uk[n-2] - 2*uk[n-1] - h2*sinh(uk[n-1]))
    return F


def jk(uk, dico):
    """
    Calcul de J_k en fonction de u^{(k)}
    """
    n = dico['n']
    X = dico['X']
    h = dico['h']
    h2 = dico['h2']
    a = [-(2 + h2 * cosh(uk[i])) for i in range(n)]
    b = [1 - h/(2*X[i]) for i in range(n-1)]
    c = [1 + h/(2*X[i]) for i in range(n-2)]
    c = [2] + c
    return a, b, c


def ukplus1(uk, z, dico):
    """
    On resout J_k( u^{(k)} - u^{(k+1)} ) = F( u^{(k)} )
    On retourne u^{(k+1)}
    """
    h = dico['h']
    h2 = dico['h2']
    X = dico['X']
    J = jk(uk, dico)
    l, v = lutri(*J)
    y = descente(l, z)
    res = remonte(v, J[2], y)
    # u^{(k+1)} = u^{(k)} - res
    return [u-r for r,u in zip(res, uk)]



def newton(mu, l=10, n=1000, plot=False):
    dico = {'h':l/n,
            'mu':mu,
            'n':n,
            'l':l}
    dico['h2'] = dico['h'] * dico['h']
    dico['X'] = [1+i*dico['h'] for i in range(dico['n'])]
    U = [debyeId(mu, n, l, plot=False)[1]]
    k = 0
    if plot:
        plt.figure()
    while k<51:
        z = F(U[k], dico)
        ukp1 = ukplus1(U[k], z, dico)
        if plot and not k%5:
            plt.plot(dico['X'], U[k], label="u_{}".format(k))
        condition1 = max(abs(f) for f in F(U[k], dico))
        condition2 = max(abs(a-b) for a,b in zip(U[k], ukp1))
        if condition1 < 10**(-12):
            # print("break1")
            if condition2 < 10**(-9):
                # print("break2")
                break
        elif condition2 < 10**(-9):
            pass
            # print("break2")
        U.append(ukp1)
        k += 1
    print("mu{}: k0=".format(mu), k)
    if plot:
        plt.legend()
        plt.savefig("muukNewton.png")
    return dico['X'], U[k]


def main():
    """
    Lancement des tests sur \mu
    """
    if len(sys.argv) == 1:
        plt.figure()
        plt.plot(*newton(1), "r-")
        plt.plot(*newton(4), "b-")
        plt.legend()
        plt.savefig("mu1et4Newton.png")
    elif int(sys.argv[1]) == 0:
        # Evolution uk
        print("--")
        if len(sys.argv) == 2:
            newton(60, plot=True)
        else:
            for i in sys.argv[2:]:
                newton(int(i), plot=True)
    else:
        deltamu = int(sys.argv[1])
        plt.figure()
        for _ in range(8):
            for i in range(deltamu):
                mu = _ + i/deltamu
                plt.plot(*newton(mu), label="mu{}".format(mu))
        plt.legend()
        plt.savefig("mu0to7NewtonpPas{}.png".format(deltamu))
    return 0

if __name__ == '__main__':
    main()
