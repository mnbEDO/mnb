#!/usr/bin/env python3

def remonte(v, c, y):
    """
    Methode de remontee pour un probleme du type
    Ux=y
    :arg v: diagonale de U
    :arg c: diagonale superieure de U
    :arg y: second membre de l'equation
    :returns x: solution du probleme Ax=z
    """
    n = len(y)
    x = [0 for _ in range(n)]
    x[-1] = y[-1] / v[-1]
    for i in range(2, n+1):
        # On se base sur le rang x_{i+1} pour
        #   calculer x_i
        x[n-i] = (y[n-i] - (c[n-i]*x[n-i + 1])) \
                / v[n-i]
    return x
