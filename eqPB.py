#!/usr/bin/env python3

from sys import argv, exit
from debye import debyeId
from math import sinh
import matplotlib.pyplot as plt


ETA1 = 10**(-12)
ETA2 = 10**(-9)

def g(X):
    """
    Fonction g(X) de la notation du tp
    """
    return [sinh(x)-x for x in X]

def ukplus1(uk, mu=1, n=1000, l=10):
    """
    renvoie le vecteur u^{k+1} en fonction du vecteur u^k
    """
    h = l / n
    h2 = h*h
    z = [h2 * gi for gi in g(uk)]
    z[0] += mu * h * (h - 2)
    X, U = debyeId(plot=False, Z=z)
    return X, U


def convergeuruk(mu, n=1000, l=10, klim=200):
    h = l / n
    h2 = h * h
    k = 0
    x, u = debyeId(mu, plot=False)
    U = [ u ]
    while (k < klim):
        X, ukp1 = ukplus1(U[k], mu)
        if k != 0:
            diff2 = max(abs(a-b) for a,b in zip(U[k], U[k-1]))
            # A*u^(k0) = g(u^(k0-1)) + un extra en position 0
            # G(u^(k0)) = g(u^(k0)) + l'extra
            # diff1 = max(abs(a-b) for a,b in zip(g(U[k-1]), g(U[k])))
            diff1 = max(h2 * abs(a - b) for a, b in zip(g(U[k - 1]), g(U[k])))
            if diff1 < ETA1:
                # print("break1", diff1)
                if diff2 < ETA2:
                    # print("break2", diff2)
                    break
            # elif diff2 < ETA2:
                # print("break2", diff2)
        if not k % 10 or k in [1, 2, 3]:
            plt.plot(X, ukp1, label="$\mu=2$, k={}".format(k))
        k += 1
        U.append(ukp1)
    print("mu{}: ".format(mu), end='')
    print("k0=", k)
    return X, U[-1]


def convergeur(mu, n=1000, l=10, mode14=False, klim=200):
    """
    Laisse tourner le schema iteratif jusqu'a CV.
    """
    h = l / n
    h2 = h * h
    k = 0
    x, u = debyeId(mu, plot=False)
    if not mode14:
        try:
            plt.subplot(2, 1, 2)
        except:
            pass
        plt.plot(x, u, "m-")
    U = [ u ]
    while (k < klim):
        X, ukp1 = ukplus1(U[k], mu)
        if k != 0:
            diff2 = max(abs(a-b) for a,b in zip(U[k], U[k-1]))
            # A*u^(k0) = g(u^(k0-1)) + un extra en position 0
            # G(u^(k0)) = g(u^(k0)) + l'extra
            diff1 = max(h2 * abs(a-b) for a,b in zip(g(U[k-1]), g(U[k])))
            if diff1 < ETA1:
                # print("break1", diff1)
                if diff2 < ETA2:
                    # print("break2", diff2)
                    break
            # elif diff2 < ETA2:
            #     print("break2", diff2)
        k += 1
        U.append(ukp1)
    print("mu{}: ".format(mu), end='')
    print("k0=", k)
    return X, U[-1]


if __name__ == '__main__':
    if len(argv) == 1:
        plt.figure()
        print("mu1: ", end='')
        convergeur(1, mode14=True)
        print("mu4: ", end='')
        convergeur(4, mode14=True)
        plt.savefig("mu1et4.png")
    elif len(argv) > 1:
        pas = int(argv[1])
        if not pas:
            plt.figure()
            convergeuruk(4.2)
            plt.legend()
            plt.savefig("muuk.png")
            exit(1)
        plt.figure()
        for mu in range(7):
            for deltamu in range(pas):
                try:
                    X, U = convergeur(mu + (deltamu/pas))
                    plt.subplot(2, 1, 1)
                    plt.plot(X, U, label="$\mu$={}".format(mu+(deltamu/pas)))
                    if pas == 1:
                        plt.legend()
                except OverflowError:
                    print("mu{}: le sinush a douillé".format(mu+(deltamu/pas)))
        print("mu{}: ".format(7), end='')
        try:
            plt.subplot(2, 1, 1)
            plt.plot(*convergeur(7), label="$\mu$=7")
            plt.legend()
        except OverflowError:
            print("le sinus a douillé")
        except:
            print("pb mu7 hors sinus")
        plt.savefig("mu0to7prec{}.png".format(argv[1]))
    else:
        print("Trop d'args en ligne de commande")
