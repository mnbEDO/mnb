#!/usr/bin/env python3

import sys as sys
from newton import newton
from math import pi

from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt



def cylindre(r):
    x, y, z = [], [], []
    for h in range(100):
        for theta in range(100):
            x.append(r * np.cos(2*pi*theta/100))
            y.append(r * np.sin(2*pi*theta/100))
            z.append(10**(-10)*h)
        plt.plot(x, y, z, "g--", alpha=0.01)

def plotCircle(r, ion, string):
    x, y, z = [], [], []
    for theta in range(100):
        x.append(r * np.cos(2*pi*theta/100))
        y.append(r * np.sin(2*pi*theta/100))
        z.append(ion)
    plt.plot(x, y, z, string, alpha=0.2)

def main():
    mu = 2.2588183421  # * (10**(9))
    R = 10**(-7)
    X, U = newton(mu)
    r = np.array([R*x for x in X])
    n0 = 2.16909193 * (10**(-9))
    ionplus =  np.array([n0 * np.exp(-u) for u in U])
    ionmoins = np.array([n0 * np.exp( u) for u in U])
    fig = plt.figure()
    ax=fig.add_subplot(111, projection='3d')
    cylindre(R)
    n = 30
    for angle in range(n):
        x, y = [], []
        for ray in r:
            x.append(ray * np.cos(2*angle*pi/n))
            y.append(ray * np.sin(2*angle*pi/n))
        plt.plot(x, y, ionplus, "r-", alpha=0.3)
        plt.plot(x, y, ionmoins, "b-", alpha=0.3)
    for i in range(len(r)):
        if not i%80:
            plotCircle(r[i], ionplus[i], "r-")
            #plotCircle(r[i], ionmoins[i], "b-")
    ax.set_title("Concentration ionique")
    plt.show()





if __name__ == '__main__':
    main()
